import os
import boto3
from botocore.exceptions import ClientError
from datetime import datetime

LOCALSTACK_ENDPOINT_URL = 'http://localhost:4566'
AWS_ACCESS_KEY_ID = 'test'
AWS_SECRET_ACCESS_KEY = 'test'

def get_s3_client():
    return boto3.client('s3', endpoint_url=LOCALSTACK_ENDPOINT_URL,
                        aws_access_key_id=AWS_ACCESS_KEY_ID,
                        aws_secret_access_key=AWS_SECRET_ACCESS_KEY)

def list_pdf_files(bucket_name):
    s3 = get_s3_client()
    paginator = s3.get_paginator('list_objects_v2')
    response_iterator = paginator.paginate(Bucket=bucket_name)

    pdf_files = []
    for page in response_iterator:
        if 'Contents' in page:
            for item in page['Contents']:
                key = item['Key']
                if is_valid_pdf_key(key):
                    pdf_files.append({'Key': key})

    return pdf_files

def is_valid_pdf_key(key):
    parts = key.split('/')
    if len(parts) == 6 and parts[-1].lower().endswith('.pdf'):
        tenant_id, year, month, day, hour, filename = parts
        try:
            year = int(year)
            day = int(day)
            hour = int(hour)
            current_year = datetime.now().year
            if 0 <= year <= current_year and month.capitalize() in ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'] and 1 <= day <= 31 and 0 <= hour <= 23:
                return True
        except ValueError:
            return False
    return False

def delete_files(bucket_name, files):
    s3 = get_s3_client()

    objects = {'Objects': files}
    failed_files = []
    try:
        response = s3.delete_objects(Bucket=bucket_name, Delete=objects)
        if 'Errors' in response:
            for obj in response['Errors']:
                failed_files.append(obj['Key'])
    except ClientError as e:
        # In case of error, handle the exception and set all files as failed
        for obj in files:
            failed_files.append(obj['Key'])

    return failed_files

def generate_tenant_report(report, tenant_id, tenant_files, failed_tenant_files):
    success_count = len(tenant_files) - len(failed_tenant_files)

    report.write("Report for " + tenant_id + ":\n")
    report.write("Deleted count: " + str(success_count) + "\n")
    if failed_tenant_files:
        report.write("Failed count: " + str(len(failed_tenant_files)) + "\n")
        report.write("Failed files:\n")
        for file in failed_tenant_files:
            report.write(file['Key'] + "\n")
    report.write("---------------------------------------------------\n")

def main():
    bucket_name = 'root'

    pdf_files = list_pdf_files(bucket_name)
    if not pdf_files:
        print("No PDF files found in the bucket.")
        return

    now = datetime.now()
    report_filename = "report_" + now.strftime('%Y-%m-%d_%H-%M') + ".txt"

    with open(report_filename, 'w') as report:
        report.write("Report date: " + now.strftime('%Y-%m-%d %H:%M') + "\n\n")
        report.flush()

        failed_files = delete_files(bucket_name, pdf_files)

        tenants = set([file['Key'].split('/')[0] for file in pdf_files])

        for tenant_id in tenants:
            tenant_files = []
            failed_tenant_files = []
            for file in pdf_files:
                if file['Key'].startswith(tenant_id + '/'):
                    tenant_files.append(file)
                    if file['Key'] in failed_files:
                        failed_tenant_files.append(file)

            generate_tenant_report(report, tenant_id, tenant_files, failed_tenant_files)

if __name__ == "__main__":
    main()