.PHONY: all help docker-up docker-logs docker-down docker-destroy populate-files list-all-files remove-all-files python-script-run

all: help

help:
	@echo "Available targets:"
	@echo "docker-up         : starts up LocalStack container with S3 enabled"
	@echo "docker-logs       : shows docker machine logs"
	@echo "docker-down       : shuts down LocalStack container"
	@echo "docker-destroy    : shuts down LocalStack, removes associated image and prunes volumes"
	@echo "populate-files    : copies sample files to a bucket named 'root'"
	@echo "list-all-files    : lists all files under the 'root' bucket"
	@echo "remove-all-files  : removes all files under the 'root' bucket"
	@echo "python-script-run : runs the s3_pdf_removal.py script"


docker-up:
		docker compose up -d

docker-logs:
		docker compose logs -f

docker-down:
		docker compose down

docker-destroy: docker-down
		docker rmi localstack/localstack:2.1.0 
		docker volume prune -f

populate-files:
		aws --endpoint-url=http://localhost:4566 s3 cp ./sample_files s3://root/ --recursive

remove-all-files:
		aws --endpoint-url=http://localhost:4566 s3 rm s3://root/ --recursive

list-all-files:
		aws --endpoint-url=http://localhost:4566 s3 ls s3://root/ --recursive

python-script-run:
		python3 python/s3_pdf_removal.py
